package ooss;

import java.util.Objects;

public class Student extends Person implements Observer {

    private int id;
    private String name;
    private int age;

    private Klass klass;

    public Student(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    public Klass getKlass() {
        return klass;
    }

    public void setKlass(Klass klass) {
        this.klass = klass;
    }

    public String introduce() {
        if (this.getKlass().isLeader(this)) {
            return String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.", this.name, this.age, this.klass.getNumber());
        }
        return String.format("My name is %s. I am %d years old. I am a student. I am in class %d.", this.name, this.age, this.klass.getNumber());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return id == student.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id);
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return this.klass != null && this.klass.getNumber() == klass.getNumber();
    }

    @Override
    public void update(String message) {
        System.out.printf("I am %s, student of Class ", this.getName());
        System.out.println(message);
    }
}
