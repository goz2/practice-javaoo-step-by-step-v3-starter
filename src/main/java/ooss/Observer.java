package ooss;

public interface Observer {
    void update(String message);
}
